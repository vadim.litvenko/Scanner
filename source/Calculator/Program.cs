﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator
{
  internal static class Program
  {
    private static void Main(string[] args)
    {
      var width = int.Parse(ConfigurationManager.AppSettings[@"Width"]);
      var height = int.Parse(ConfigurationManager.AppSettings[@"Height"]);

      var matrix = new bool[width, width, height];

      var startIndex = int.Parse(ConfigurationManager.AppSettings[@"AnglePatternStartIndex"]);
      var length = int.Parse(ConfigurationManager.AppSettings[@"AnglePatternLength"]);
      var inputFolder = ConfigurationManager.AppSettings[@"InputDirectory"];
      var inputFileNames = Directory.GetFiles(inputFolder);

      foreach (var inputFileName in inputFileNames)
      {
        var anglePattern = Path.GetFileName(inputFileName).Substring(startIndex, length);
        var angle = double.Parse(anglePattern) * Math.PI / 180.0;
        var cos = Math.Cos(angle);
        var sin = Math.Sin(angle);
        var center = (width - 1) * 0.5;
        var add = (1.0 - cos - sin) * center;

        using (var reader = File.OpenText(inputFileName))
        {
          for (var k = 0; k < height; k++)
          {
            var values = reader.ReadLine().Select(x => x == '0').ToArray();

            Parallel.For(0, width, j =>
            {
              for (var i = 0; i < width; i++)
              {
                if (matrix[i, j, k])
                {
                  continue;
                }

                var index = (int)Math.Round(cos * i + sin * j + add);

                if (0 <= index && index < width)
                {
                  matrix[i, j, k] = values[index];
                }
                else
                {
                  matrix[i, j, k] = true;
                }
              }
            });
          }
        }
      }

      // Return result
      var outputFolder = ConfigurationManager.AppSettings[@"OutputDirectory"];

      if (!Directory.Exists(outputFolder))
      {
        Directory.CreateDirectory(outputFolder);
      }

      var outputFileName = string.Format(@"{0}.model", DateTime.Now.ToString(@"yyyy-MM-dd.HH-mm-ss"));
      var outputPath = Path.Combine(outputFolder, outputFileName);

      using (var writer = File.CreateText(outputPath))
      {
        for (var k = 0; k < height; k++)
        {
          for (var j = 0; j < width; j++)
          {
            for (var i = 0; i < width; i++)
            {
              writer.Write(matrix[i, j, k] ? '0' : '1');
            }

            writer.Write('\n');
          }

          writer.Write('\n');
        }
      }
    }
  }
}
