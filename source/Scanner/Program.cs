﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Media;
using System.Threading;

using AForge.Video;
using AForge.Video.DirectShow;

namespace Scanner
{
  internal static class Program
  {
    private static Bitmap _frame;
    private static readonly object FrameLock = new object();

    private static void Main(string[] args)
    {
      // Initialize communication
      var communicator = new Communicator();

      communicator.Initialize();

      // Initialize camera
      var devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
      var deviceIndex = int.Parse(ConfigurationManager.AppSettings[@"DeviceIndex"]);
      var device = new VideoCaptureDevice(devices[deviceIndex].MonikerString);

      device.NewFrame += OnNewFrame;

      device.Start();

      Thread.Sleep(5000);

      // Create output folder
      var folder = ConfigurationManager.AppSettings[@"OutputDirectory"];

      if (!Directory.Exists(folder))
      {
        Directory.CreateDirectory(folder);
      }

      // Create images
      var fileNamePattern = ConfigurationManager.AppSettings[@"OutputFileNamePattern"];
      var step = double.Parse(ConfigurationManager.AppSettings[@"Step"], CultureInfo.InvariantCulture);
      var ratio = double.Parse(ConfigurationManager.AppSettings[@"Ratio"], CultureInfo.InvariantCulture);
      var speed = double.Parse(ConfigurationManager.AppSettings[@"Speed"], CultureInfo.InvariantCulture);

      for (var angle = 0.0; angle < 180.0; angle += step)
      {
        if (angle > 0.0)
        {
          communicator.SendCommand(string.Format(@"G0 R{0:F0} S{1:F0}", step * ratio, speed));

          var answer = communicator.WaitForAnswer();

          if (answer != @"OK")
          {
            throw new Exception(answer);
          }
        }

        // Save image
        Bitmap frame;

        lock (FrameLock)
        {
          frame = _frame;
        }

        var fileName = string.Format(fileNamePattern, angle);
        var path = Path.Combine(folder, fileName);

        frame.Save(path, ImageFormat.Bmp);

        SystemSounds.Beep.Play();
      }

      device.Stop();
    }

    private static void OnNewFrame(Object sender, NewFrameEventArgs e)
    {
      lock (FrameLock)
      {
        _frame = (Bitmap)e.Frame.Clone();
      }
    }
  }
}
