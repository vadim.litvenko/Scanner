﻿using System.Configuration;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace Scanner
{
  internal class Communicator
  {
    private SerialPort _serialPort;
    private string _buffer = string.Empty;
    private string _answer = string.Empty;
    private readonly ManualResetEvent _event = new ManualResetEvent(true);

    private void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      while (_serialPort.BytesToRead > 0)
      {
        var buffer = new byte[_serialPort.BytesToRead];

        var length = _serialPort.Read(buffer, 0, buffer.Length);

        _buffer = string.Concat(_buffer, Encoding.ASCII.GetString(buffer, 0, length));
      }

      for (var i = _buffer.IndexOf('\n'); i != -1; i = _buffer.IndexOf('\n'))
      {
        _answer = _buffer.Substring(0, i - 1);

        _buffer = _buffer.Remove(0, i + 1);

        _event.Set();
      }
    }

    public void Initialize()
    {
      var portName = ConfigurationManager.AppSettings[@"PortName"];
      var portBaudRate = int.Parse(ConfigurationManager.AppSettings[@"PortBaudRate"]);

      _serialPort = new SerialPort(portName, portBaudRate) { DtrEnable = true, RtsEnable = true };

      _serialPort.DataReceived += OnDataReceived;

      _serialPort.Open();

      Thread.Sleep(1000);
    }

    public void SendCommand(string line)
    {
      _serialPort.WriteLine(line);
    }

    public string WaitForAnswer()
    {
      _event.Reset();

      _event.WaitOne();

      return _answer;
    }
  }
}
