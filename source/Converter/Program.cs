﻿using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using AForge.Imaging;
using AForge.Imaging.Filters;

namespace Converter
{
  internal static class Program
  {
    private static void Main(string[] args)
    {
      // Create filter
      var r = byte.Parse(ConfigurationManager.AppSettings[@"KeyColorR"]);
      var g = byte.Parse(ConfigurationManager.AppSettings[@"KeyColorG"]);
      var b = byte.Parse(ConfigurationManager.AppSettings[@"KeyColorB"]);

      var centerColor = new RGB(r, g, b);

      var radius = short.Parse(ConfigurationManager.AppSettings[@"Radius"]);

      var filter = new EuclideanColorFiltering { Radius = radius, CenterColor = centerColor, FillColor = centerColor, FillOutside = false };

      // Create resize filter
      var width = int.Parse(ConfigurationManager.AppSettings[@"Width"]);
      var height = int.Parse(ConfigurationManager.AppSettings[@"Height"]);

      var resize = new ResizeBicubic(width, height);

      // Create output folder
      var outputFolder = ConfigurationManager.AppSettings[@"OutputDirectory"];

      if (!Directory.Exists(outputFolder))
      {
        Directory.CreateDirectory(outputFolder);
      }

      // Create debug output folder
      var debugOutputFolder = ConfigurationManager.AppSettings[@"DebugOutputDirectory"];

      if (!Directory.Exists(debugOutputFolder))
      {
        Directory.CreateDirectory(debugOutputFolder);
      }

      // Read images
      var inputFolder = ConfigurationManager.AppSettings[@"InputDirectory"];
      var inputFileNames = Directory.GetFiles(inputFolder);

      foreach (var inputFileName in inputFileNames)
      {
        var image = new Bitmap(inputFileName);

        // Resize
        image = resize.Apply(image);

        // Binarization
        filter.ApplyInPlace(image);

        // Save debug
        var debugOutputFileName = Path.GetFileName(inputFileName);
        var debugPath = Path.Combine(debugOutputFolder, debugOutputFileName);

        image.Save(debugPath, ImageFormat.Bmp);

        // Save matrix
        var outputFileName = string.Format(@"{0}.matrix", Path.GetFileName(inputFileName));
        var path = Path.Combine(outputFolder, outputFileName);

        using (var writer = File.CreateText(path))
        {
          for (var j = 0; j < image.Height; j++)
          {
            for (var i = 0; i < image.Width; i++)
            {
              var pixel = image.GetPixel(i, j);

              writer.Write(pixel.R == centerColor.Red && pixel.G == centerColor.Green && pixel.B == centerColor.Blue ? 0 : 1);
            }

            writer.Write('\n');
          }
        }
      }
    }
  }
}
