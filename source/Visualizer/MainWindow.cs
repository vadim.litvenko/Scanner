﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Visualizer
{
  internal sealed class MainWindow : GameWindow
  {
    private int _fileIndex;
    private string _fileName;
    private List<List<List<bool>>> _matrix;
    private List<Tuple<int, int, int>> _voxels;
    private int _countX;
    private int _countY;
    private int _countZ;
    private float _rotationX;
    private float _rotationZ;
    private float _scale = 1.0f;

    public MainWindow()
    {
      LoadMatrix();
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      GL.Enable(EnableCap.DepthTest);
      GL.Enable(EnableCap.Normalize);
      GL.Enable(EnableCap.Lighting);

      // Enable light 0
      float[] light0Position = { -1.0f, 0.0f, 1.0f, 0.0f };
      float[] light0Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
      float[] light0Diffuse = { 0.7f, 0.3f, 0.3f, 1.0f };

      GL.Light(LightName.Light0, LightParameter.Position, light0Position);
      GL.Light(LightName.Light0, LightParameter.Ambient, light0Ambient);
      GL.Light(LightName.Light0, LightParameter.Diffuse, light0Diffuse);

      GL.Enable(EnableCap.Light0);

      // Enable light 1
      float[] light1Position = { 1.0f, 0.0f, 1.0f, 0.0f };
      float[] light1Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
      float[] light1Diffuse = { 0.3f, 0.7f, 0.3f, 1.0f };

      GL.Light(LightName.Light1, LightParameter.Position, light1Position);
      GL.Light(LightName.Light1, LightParameter.Ambient, light1Ambient);
      GL.Light(LightName.Light1, LightParameter.Diffuse, light1Diffuse);

      GL.Enable(EnableCap.Light1);

      // Enable light 2
      float[] light2Position = { 0.0f, 1.0f, 0.0f, 0.0f };
      float[] light2Ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
      float[] light2Diffuse = { 0.3f, 0.3f, 0.7f, 1.0f };

      GL.Light(LightName.Light2, LightParameter.Position, light2Position);
      GL.Light(LightName.Light2, LightParameter.Ambient, light2Ambient);
      GL.Light(LightName.Light2, LightParameter.Diffuse, light2Diffuse);

      GL.Enable(EnableCap.Light2);
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);

      GL.Viewport(0, 0, Width, Height);

      // Set projection transformation
      var aspect = (float)Width / Height;
      var perspective = Matrix4.CreatePerspectiveFieldOfView(1.0f, aspect, 1.0f, 5.0f);

      GL.MatrixMode(MatrixMode.Projection);

      GL.LoadIdentity();

      GL.MultMatrix(ref perspective);
    }

    protected override void OnKeyDown(KeyboardKeyEventArgs e)
    {
      if (e.Key == Key.Escape && !e.IsRepeat)
      {
        Exit();
      }

      if (e.Key == Key.PageUp && !e.IsRepeat)
      {
        _fileIndex -= 1;

        LoadMatrix();
      }

      if (e.Key == Key.PageDown && !e.IsRepeat)
      {
        _fileIndex += 1;

        LoadMatrix();
      }
    }

    protected override void OnUpdateFrame(FrameEventArgs e)
    {
      base.OnUpdateFrame(e);

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.Up))
      {
        _rotationX -= 1.0f;
      }

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.Down))
      {
        _rotationX += 1.0f;
      }

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.Left))
      {
        _rotationZ -= 1.0f;
      }

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.Right))
      {
        _rotationZ += 1.0f;
      }

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.KeypadPlus))
      {
        _scale *= 1.01f;
      }

      if (OpenTK.Input.Keyboard.GetState(0).IsKeyDown(Key.KeypadMinus))
      {
        _scale *= 0.99f;
      }

      Title = string.Format(@"{0} | {1} | pitch = {2} yaw = {3} scale = {4}", _fileIndex, _fileName, _rotationX, _rotationZ, _scale);
    }

    protected override void OnRenderFrame(FrameEventArgs e)
    {
      base.OnRenderFrame(e);

      GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

      var lookat = Matrix4.LookAt(0.0f, -3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

      GL.MatrixMode(MatrixMode.Modelview);

      GL.LoadIdentity();

      GL.MultMatrix(ref lookat);

      GL.Rotate(_rotationX, 1.0f, 0.0f, 0.0f);
      GL.Rotate(_rotationZ, 0.0f, 0.0f, 1.0f);
      GL.Scale(_scale, _scale, _scale);

      DrawVoxels();

      SwapBuffers();
    }

    private void DrawVoxels()
    {
      if (_voxels == null)
      {
        return;
      }

      var factor = 1.0f / _matrix.Count;

      foreach (var voxel in _voxels)
      {
        DrawVoxel(
          (voxel.Item1 - _countX / 2) * factor,
          (_countY / 2 - voxel.Item2) * factor,
          (_countZ / 2 - voxel.Item3) * factor, factor, Color.White);
      }
    }

    private void DrawVoxel(float x, float y, float z, float size, Color color)
    {
      size *= 0.5f;

      GL.Begin(PrimitiveType.Quads);

      // Front
      GL.Color3(color);

      GL.Normal3(0.0f, 0.0f, 1.0f);

      GL.Vertex3(x + size, y + size, z + size);
      GL.Vertex3(x - size, y + size, z + size);
      GL.Vertex3(x - size, y - size, z + size);
      GL.Vertex3(x + size, y - size, z + size);

      // Back
      GL.Color3(color);

      GL.Normal3(0.0f, 0.0f, -1.0f);

      GL.Vertex3(x + size, y + size, z - size);
      GL.Vertex3(x + size, y - size, z - size);
      GL.Vertex3(x - size, y - size, z - size);
      GL.Vertex3(x - size, y + size, z - size);

      // Left
      GL.Color3(color);

      GL.Normal3(-1.0f, 0.0f, 0.0f);

      GL.Vertex3(x - size, y + size, z + size);
      GL.Vertex3(x - size, y + size, z - size);
      GL.Vertex3(x - size, y - size, z - size);
      GL.Vertex3(x - size, y - size, z + size);

      // Right
      GL.Color3(color);

      GL.Normal3(1.0f, 0.0f, 0.0f);

      GL.Vertex3(x + size, y + size, z + size);
      GL.Vertex3(x + size, y - size, z + size);
      GL.Vertex3(x + size, y - size, z - size);
      GL.Vertex3(x + size, y + size, z - size);

      // Top
      GL.Color3(color);

      GL.Normal3(0.0f, 1.0f, 0.0f);

      GL.Vertex3(x + size, y + size, z + size);
      GL.Vertex3(x + size, y + size, z - size);
      GL.Vertex3(x - size, y + size, z - size);
      GL.Vertex3(x - size, y + size, z + size);

      // Bottom
      GL.Color3(color);

      GL.Normal3(0.0f, -1.0f, 0.0f);

      GL.Vertex3(x + size, y - size, z + size);
      GL.Vertex3(x - size, y - size, z + size);
      GL.Vertex3(x - size, y - size, z - size);
      GL.Vertex3(x + size, y - size, z - size);

      GL.End();
    }

    private void LoadMatrix()
    {
      _matrix = new List<List<List<bool>>> { new List<List<bool>>() };
      _voxels = new List<Tuple<int, int, int>>();
      _countX = _countY = _countZ = 0;
      _fileName = null;

      var inputFolder = ConfigurationManager.AppSettings[@"InputDirectory"];
      var inputFileNames = Directory.GetFiles(inputFolder);

      if (_fileIndex < 0 || _fileIndex >= inputFileNames.Length)
      {
        return;
      }

      _fileName = inputFileNames[_fileIndex];

      using (var reader = File.OpenText(_fileName))
      {
        var addNewLayer = false;

        while (!reader.EndOfStream)
        {
          var line = reader.ReadLine();

          if (string.IsNullOrEmpty(line))
          {
            addNewLayer = true;

            continue;
          }

          if (addNewLayer)
          {
            _countY = Math.Max(_matrix.Last().Count, _countY);

            _matrix.Add(new List<List<bool>>());

            addNewLayer = false;
          }

          var row = line.Select(x => x == '1').ToList();

          _countX = Math.Max(row.Count, _countX);

          _matrix.Last().Add(row);
        }
      }

      _countZ = _matrix.Count;

      // Get voxels
      for (var k = 0; k < _matrix.Count; k++)
      {
        var layer = _matrix[k];

        for (var j = 0; j < layer.Count; j++)
        {
          var row = layer[j];

          for (var i = 0; i < row.Count; i++)
          {
            var value = row[i];

            if (!value ||
                i != 0 && i != row.Count - 1 &&
                j != 0 && j != layer.Count - 1 &&
                k != 0 && k != _matrix.Count - 1 &&
                _matrix[k][j][i - 1] && _matrix[k][j][i + 1] &&
                _matrix[k][j - 1][i] && _matrix[k][j + 1][i] &&
                _matrix[k - 1][j][i] && _matrix[k + 1][j][i])
            {
              continue;
            }

            _voxels.Add(new Tuple<int, int, int>(i, j, k));
          }
        }
      }
    }
  }
}
