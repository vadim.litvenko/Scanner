﻿namespace Visualizer
{
  internal static class Program
  {
    private static void Main(string[] args)
    {
      using (var window = new MainWindow())
      {
        window.Run(30);
      }
    }
  }
}
