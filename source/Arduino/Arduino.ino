const int IN1 = 2;
const int IN2 = 3;
const int IN3 = 4;
const int IN4 = 5;

const long stepsPerRevolution = 4096L;

const int stepCount = 8;
const int stepMasks[stepCount] = { 1, 3, 2, 6, 4, 12, 8, 9 };

int currentStepIndex = 0;

void delay2(long us)
{
  if (us <= 16383)
  {
    delayMicroseconds(us);
  }
  else
  {
    delay(us / 1000L);
  }
}

void setStep(int mask)
{
  digitalWrite(IN1, mask & 0x1 ? HIGH : LOW);
  digitalWrite(IN2, mask & 0x2 ? HIGH : LOW);
  digitalWrite(IN3, mask & 0x4 ? HIGH : LOW);
  digitalWrite(IN4, mask & 0x8 ? HIGH : LOW);
}

void step(long steps, long speed)
{
  long n = steps < 0 ? -steps : steps;
  long d = 60000000L / (stepsPerRevolution*speed);

  for (long i = 0L; i < n; i++)
  {
    if (steps < 0)
    {
      currentStepIndex--;

      if (currentStepIndex < 0)
      {
        currentStepIndex = stepCount - 1;
      }
    }
    else
    {
      currentStepIndex = (currentStepIndex + 1) % stepCount;
    }

    setStep(stepMasks[currentStepIndex]);

    delay2(d);
  }
}

boolean readDegrees(long &degrees, long &speed)
{
  if (Serial.available() == 0)
  {
    return false;
  }

  String line = Serial.readStringUntil('\n');

  if (line.startsWith("G0"))
  {
    {
      int xBeginIndex = line.indexOf('R') + 1;
      int xEndIndex = line.indexOf(' ', xBeginIndex);
      degrees = line.substring(xBeginIndex, xEndIndex).toInt();
    }

    {
      int xBeginIndex = line.indexOf('S') + 1;
      int xEndIndex = line.indexOf(' ', xBeginIndex);
      speed = line.substring(xBeginIndex, xEndIndex).toInt();
    }

    return true;
  }

  Serial.println(F("ERR"));
  Serial.flush();

  return false;
}

void setup()
{
  Serial.begin(9600);

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
}

void loop()
{
  long degrees, speed;

  if (readDegrees(degrees, speed))
  {
    if (degrees != 0 && speed != 0)
    {
      long steps = (degrees*stepsPerRevolution) / 360L;

      step(steps, speed);
    }

    Serial.println(F("OK"));
    Serial.flush();
  }
}
